package eu.similarity.msc.search;

import java.util.ArrayList;
import java.util.List;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import eu.similarity.msc.util.ObjectWithDistance;
import eu.similarity.msc.util.Quicksort;

public class VPTree<T> {

	private final class VPTreeNode {
		private T pivot;
		private double mu;
		private VPTreeNode left;
		private VPTreeNode right;

		VPTreeNode(List<T> data, Metric<T> metric) {
			if (data.size() == 0) {
				// should not happen
			} else if (data.size() == 1) {
				this.pivot = data.get(0);
			} else {
				this.pivot = data.get(0);
				@SuppressWarnings("unchecked")
				ObjectWithDistance<T>[] objs = new ObjectWithDistance[data.size() - 1];
				int ptr = 0;
				for (T datum : data.subList(1, data.size())) {
					objs[ptr++] = new ObjectWithDistance<>(datum, metric.distance(this.pivot, datum));
				}
				Quicksort.placeMedian(objs);

				List<T> leftList = new ArrayList<>();
				for (int i = 0; i < objs.length / 2; i++) {
					leftList.add(objs[i].getValue());
				}
				this.mu = objs[objs.length / 2].getDistance();
				List<T> rightList = new ArrayList<>();
				for (int i = objs.length / 2; i < objs.length; i++) {
					rightList.add(objs[i].getValue());
				}
				if (leftList.size() > 0) {
					this.left = new VPTreeNode(leftList, metric);
				}
				if (rightList.size() > 0) {
					this.right = new VPTreeNode(rightList, metric);
				}
			}
		}

		public void search(T query, double t, List<T> results) {
			@SuppressWarnings("synthetic-access")
			double pq = VPTree.this.metric.distance(this.pivot, query);
			if (pq <= t) {
				results.add(this.pivot);
			}
			if (this.left != null && !canExclude(t, pq, this.mu)) {
				this.left.search(query, t, results);
			}
			if (this.right != null && !canExclude(pq, this.mu, t)) {
				this.right.search(query, t, results);
			}
		}

	}

	private VPTreeNode index;
	private Metric<T> metric;

	public VPTree(List<T> data, Metric<T> metric) {
		this.metric = metric;
		this.index = new VPTreeNode(data, metric);
	}

	public List<T> search(T query, double threshold) {
		List<T> res = new ArrayList<>();
		this.index.search(query, threshold, res);
		return res;
	}

	@SuppressWarnings("static-method")
	protected boolean canExclude(double a, double b, double c) {
		return b >= c + a;
	}

	public List<Integer> nearestNeighbour(T q, int noOfNN) {
		// TODO
		throw new RuntimeException("uh oh, shouldn't have called this yet!");
	}

	public int nearestNeighbour(CartesianPoint q) {
		// TODO
		throw new RuntimeException("uh oh, shouldn't have called this yet!");
	}

}
