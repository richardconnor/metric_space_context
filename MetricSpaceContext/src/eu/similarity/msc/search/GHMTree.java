package eu.similarity.msc.search;

import java.util.ArrayList;
import java.util.List;

import eu.similarity.msc.core_concepts.Metric;

/**
 * @author Richard Connor
 *
 *         a monotonous generalised hyperplane tree
 */
public class GHMTree<T> {

	private class TreeNode {

		double lCR, rCR;
		double l_r_dist;
		T lHead, rHead;
		TreeNode lTree, rTree;

		TreeNode(T upperNode, List<T> data) {
			this.lCR = 0;
			this.rCR = 0;
			if (data.size() == 0) {
				// shouldn't really happen...
			} else if (data.size() == 1) {
				this.lHead = data.get(0);
			} else if (data.size() == 2) {
				// no point in building sublists...
				this.lHead = data.get(0);
				this.rHead = data.get(1);
			} else {
				// more than two nodes in data so requires recursion
				// upperNode is null if and only if this is the head node of the tree
				this.lHead = upperNode != null ? upperNode : data.get(1);
				this.rHead = data.get(0);
				this.l_r_dist = GHMTree.this.metric.distance(this.lHead, this.rHead);

				List<T> lList = new ArrayList<>();
				List<T> rList = new ArrayList<>();
				for (T d : data.subList(upperNode != null ? 1 : 2, data.size())) {
					double d1 = GHMTree.this.metric.distance(d, this.lHead);
					double d2 = GHMTree.this.metric.distance(d, this.rHead);
					if (d1 < d2) {
						lList.add(d);
						this.lCR = Math.max(this.lCR, d1);
					} else {
						rList.add(d);
						this.rCR = Math.max(this.rCR, d2);
					}
				}

				if (lList.size() > 0) {
					this.lTree = new TreeNode(this.lHead, lList);
				}
				if (rList.size() > 0) {
					this.rTree = new TreeNode(this.rHead, rList);
				}
			}

		}

		/**
		 * @param query the query itself
		 * @param t     the threshold
		 * @param res   result list to be added to by side-effect
		 * @param dMin  the smallest distance that's been encountered above this node of
		 *              the tree
		 */
		@SuppressWarnings("synthetic-access")
		public void search(T query, double t, double upperDist, List<T> res) {
			double dLeft, dRight;
			if (this.lHead == null) {
				// shouldn't happen
			} else if (this.rHead == null) {
				if (GHMTree.this.metric.distance(query, this.lHead) <= t) {
					res.add(this.lHead);
				}
			} else if (this.lTree == null && this.rTree == null) {
				// needed for the two-data leaf nodes
				if (GHMTree.this.metric.distance(query, this.lHead) <= t) {
					res.add(this.lHead);
				}
				if (GHMTree.this.metric.distance(query, this.rHead) <= t) {
					res.add(this.rHead);
				}
			} else {
				dLeft = upperDist >= 0 ? upperDist : GHMTree.this.metric.distance(query, this.lHead);
				dRight = GHMTree.this.metric.distance(query, this.rHead);
				boolean lClosest = dLeft < dRight;

				if (dRight <= t) {
					res.add(this.rHead);
				}
				if (upperDist < 0 && dLeft <= t) {
					res.add(this.lHead);
				}
				if (this.lTree != null) {
					if (!excludeCR(dLeft, t, this.lCR) && !(!lClosest && excludeVor(dLeft, dRight, t))
							&& !(!lClosest && excludeHilbert(this.l_r_dist, dRight, dLeft, t))) {

						this.lTree.search(query, t, dLeft, res);
					}
				}

				if (this.rTree != null) {
					if (!excludeCR(dRight, t, this.rCR) && !(lClosest && excludeVor(dRight, dLeft, t))
							&& !(lClosest && excludeHilbert(this.l_r_dist, dLeft, dRight, t))) {

						this.rTree.search(query, t, dRight, res);
					}
				}
			}
		}

	}

	protected List<T> data;
	protected Metric<T> metric;
	private TreeNode head;
	private boolean crExclusionEnabled;
	private boolean vorExclusionEnabled;
	private boolean cosExclusionEnabled;

	public GHMTree(List<T> data, Metric<T> metric) {
		this.data = data;
		this.metric = metric;

		this.head = new TreeNode(null, data);

		this.cosExclusionEnabled = true;
		this.crExclusionEnabled = true;
		this.vorExclusionEnabled = true;
	}

	/**
	 * @param cosExclusionEnabled the cosExclusionEnabled to set
	 */
	public void setHilbertExclusionEnabled(boolean cosExclusionEnabled) {
		this.cosExclusionEnabled = cosExclusionEnabled;
	}

	/**
	 * @param crExclusionEnabled the crExclusionEnabled to set
	 */
	public void setCrExclusionEnabled(boolean crExclusionEnabled) {
		this.crExclusionEnabled = crExclusionEnabled;
	}

	/**
	 * @param vorExclusionEnabled the vorExclusionEnabled to set
	 */
	public void setVorExclusionEnabled(boolean vorExclusionEnabled) {
		this.vorExclusionEnabled = vorExclusionEnabled;
	}

	public List<T> search(T query, double t) {
		List<T> res = new ArrayList<>();
		this.head.search(query, t, -1, res);
		return res;
	}

	private boolean excludeHilbert(double p, double c, double b, double threshold) {
		if (this.cosExclusionEnabled) {
			double cosTheta = (p * p + c * c - b * b) / (2 * p * c);
			double projection = c * cosTheta;
			return ((p / 2) - projection) > threshold;
		} else {
			return false;
		}
	}

	private boolean excludeCR(double dPivot, double threshold, double coveringRadius) {
		return this.crExclusionEnabled && dPivot > coveringRadius + threshold;
	}

	private boolean excludeVor(double dLarge, double dSmall, double threshold) {
		return this.vorExclusionEnabled && dLarge - dSmall > threshold * 2;
	}

}
