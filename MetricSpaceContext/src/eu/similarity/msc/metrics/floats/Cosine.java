package eu.similarity.msc.metrics.floats;

import eu.similarity.msc.core_concepts.Metric;

public class Cosine implements Metric<float[]> {

	@Override
	public double distance(float[] x, float[] y) {
		double xProduct = 0;
		for (int i = 0; i < x.length; i++) {
			xProduct += x[i] * y[i];
		}
		double cosTheta = xProduct / (magnitude(x) * magnitude(y));

		return 1 - cosTheta;
	}

	private static double magnitude(float[] x) {
		double acc = 0;
		for (float f : x) {
			acc += f * f;
		}
		return Math.sqrt(acc);
	}

	@Override
	public String getMetricName() {
		return "Cos";
	}

}
