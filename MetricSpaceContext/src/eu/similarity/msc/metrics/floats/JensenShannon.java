package eu.similarity.msc.metrics.floats;

import java.util.logging.Logger;

import eu.similarity.msc.core_concepts.Metric;

/**
 * @author Richard Connor
 * 
 *         this class assumes the input vectors are L1-normalised, and will
 *         probably go horribly wrong if they're not
 *
 */
public class JensenShannon implements Metric<float[]> {

	@Override
	public double distance(float[] x, float[] y) {

		double accumulator = 0;
		int ptr = 0;
		for (double d1 : x) {
			double d2 = y[ptr++];

			if (d1 != 0 && d2 != 0) {
				accumulator -= xLogx(d1);
				accumulator -= xLogx(d2);
				accumulator += xLogx(d1 + d2);
				if (Double.isNaN(accumulator)) {
					throw new RuntimeException("accumulator has become NaN: d1 is " + d1 + " and d2 is " + d2);
				}
			}

		}
		/*
		 * allow for rounding errors, if this goes over 1.0 very bad things might
		 * happen!
		 */
		final double acc = Math.min(accumulator / (Math.log(2) * 2), 1);

		final double res = Math.sqrt(1 - acc);
		if (Double.isNaN(res)) {
			Logger.getLogger(this.getClass().getName()).severe("nan result (acc = " + acc + ")");
			throw new RuntimeException("nan result: acc = " + acc + ", accumulator = " + accumulator);
		}
		return res;
	}

	private static double xLogx(double d) {
		return d * Math.log(d);
	}

	@Override
	public String getMetricName() {
		return "jsd";
	}
}
