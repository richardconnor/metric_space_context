package eu.similarity.msc.metrics.floats;

import eu.similarity.msc.core_concepts.Metric;

public class L2Norm implements Metric<float[]> {

	@Override
	public double distance(float[] x, float[] y) {
		double magX = magnitude(x);
		double magY = magnitude(y);
		double acc = 0;
		for (int i = 0; i < x.length; i++) {
			double diff = x[i] / magX - y[i] / magY;
			acc += diff * diff;
		}
		return Math.sqrt(acc);
	}
	
	private static double magnitude(float[] x) {
		double acc = 0;
		for (float f : x) {
			acc += f * f;
		}
		return Math.sqrt(acc);
	}

	@Override
	public String getMetricName() {
		return "Euc";
	}

}
