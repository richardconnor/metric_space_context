package eu.similarity.msc.metrics.floats;

import java.util.logging.Logger;

import eu.similarity.msc.core_concepts.Metric;

/**
 * @author Richard Connor
 * 
 *         this class assumes the input vectors are L1-normalised, and will
 *         probably go horribly wrong if they're not
 *
 */
public class Triangular implements Metric<float[]> {

	@Override
	public double distance(float[] x, float[] y) {

		double accumulator = 0;
		int ptr = 0;
		for (int i = 0; i < x.length; i++) {
			double d1 = x[i];
			double d2 = y[i];

			if (!(d1 == 0 && d2 == 0)) {
				double diff = d1 - d2;
				diff = diff * diff;
				accumulator += diff / (d1 + d2);
			}
		}

		return Math.sqrt(accumulator / 2);
	}

	@Override
	public String getMetricName() {
		return "tri";
	}
}
