package eu.similarity.msc.metrics.ints;

import eu.similarity.msc.core_concepts.Metric;

public class Manhattan implements Metric<int[]> {

	@Override
	public double distance(int[] x, int[] y) {
		double acc = 0;
		for (int i = 0; i < x.length; i++) {
			double diff = x[i] - y[i];
			acc += Math.abs(diff);
		}
		return acc;
	}

	@Override
	public String getMetricName() {
		return "Man";
	}

}
