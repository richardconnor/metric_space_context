package eu.similarity.msc.metrics.doubles;

import eu.similarity.msc.core_concepts.Metric;

public class Cosine implements Metric<double[]> {

	@Override
	public double distance(double[] x, double[] y) {
		double xProduct = 0;
		for (int i = 0; i < x.length; i++) {
			xProduct += x[i] * y[i];
		}
		double cosTheta = xProduct / (magnitude(x) * magnitude(y));

		return 1 - cosTheta;
	}

	private static double magnitude(double[] x) {
		double acc = 0;
		for (double f : x) {
			acc += f * f;
		}
		return Math.sqrt(acc);
	}

	@Override
	public String getMetricName() {
		return "Cos";
	}

}
