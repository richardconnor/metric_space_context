package eu.similarity.msc.data;

import java.util.Map;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.floats.Euclidean;

public class GistEucMetricSpace extends IncrementalBuildMetricSpace<Integer, float[]> {

	public GistEucMetricSpace(String filePath) {
		super(filePath);
	}

	@Override
	public Metric<float[]> getMetric() {
		return new Euclidean();
	}

	@Override
	protected Map<Integer, float[]> getRawDataHunk(int fileNumber) {
		throw new RuntimeException("data already in place");
	}
}
