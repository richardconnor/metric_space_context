package eu.similarity.msc.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import eu.similarity.msc.core_concepts.CountedMetric;
import eu.similarity.msc.core_concepts.Metric;

public class DataListView {

	public static class IdDatumPair<K, T> {
		public K id;
		public T datum;

		public IdDatumPair(K id, T datum) {
			this.id = id;
			this.datum = datum;
		}
	}

	public static <T> List<T> removeRandom(List<T> data, int i) {
		Random rand = new Random(0);
		List<T> res = new ArrayList<>();
		while (res.size() < i) {
			res.add(data.remove(rand.nextInt(data.size())));
		}
		return res;
	}

	public static <K, T> List<IdDatumPair<K, T>> convert(Map<K, T> data) {
		List<IdDatumPair<K, T>> res = new ArrayList<>();
		for (K i : data.keySet()) {
			IdDatumPair<K, T> idp = new IdDatumPair<>(i, data.get(i));
			res.add(idp);
		}
		return res;
	}

	public static <K, T> CountedMetric<IdDatumPair<K, T>> convert(final Metric<T> m) {
		Metric<IdDatumPair<K, T>> met = new Metric<IdDatumPair<K, T>>() {

			@Override
			public double distance(IdDatumPair<K, T> x, IdDatumPair<K, T> y) {
				return m.distance(x.datum, y.datum);
			}

			@Override
			public String getMetricName() {
				return m.getMetricName();
			}
		};
		return new CountedMetric<>(met);
	}
}
