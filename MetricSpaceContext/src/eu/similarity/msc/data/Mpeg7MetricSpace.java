package eu.similarity.msc.data;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Logger;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.ints.Manhattan;

/**
 * @author Richard Connor
 */
public class Mpeg7MetricSpace extends IncrementalBuildMetricSpace<Integer, int[]> {

	private static int[] getDataFromNextLines(LineNumberReader fr) throws Exception {
		int[] data = new int[282];
		int dim = 0;
		int linesRead = 0;
		while (linesRead < 5) {
//		for (int i = 0; i < 5; i++) {
			Scanner s1 = new Scanner(fr.readLine());
			if (linesRead == 4) {
				// some data have an extra line with a couple of floats in it... discard them!
				if (s1.findInLine("[0-9]\\.[0-9]") != null) {
					s1.close();
					s1 = new Scanner(fr.readLine());
				}
			}
			s1.useDelimiter("[,;]\\s");

			while (s1.hasNextInt()) {
				int n = s1.nextInt();
				data[dim++] = n;
			}

			s1.close();
			linesRead++;
		}
		if (dim < 282) {
			throw new Exception(dim + " is not enough data");
		}
		return data;
	}

	private static int getIdFromNextLine(LineNumberReader fr) throws Exception {
		final String line = fr.readLine();
		Scanner s = new Scanner(line);
		if (!"#objectKey".equals(s.next())) {
			s.close();
			if (line != null) {
				System.out.println("last line: " + line);
			}
			throw new Exception("no more data");
		}
		s.next();
		int id = s.nextInt();
		s.close();
		return id;
	}

	private Map<Integer, int[]> rawData;
	private List<Integer> dataIds;

	public Mpeg7MetricSpace(String filePath) {
		super(filePath);
	}

	public Mpeg7MetricSpace(String filePath, String rawFilePath) {
		super(filePath);
		try {
			this.dataIds = new ArrayList<>();
			this.rawData = getMpeg7(new File(rawFilePath));
		} catch (IOException e) {
			Logger.getLogger(this.getClass().getName()).severe("can't open file: " + rawFilePath);
			throw new RuntimeException();
		}
	}

	@Override
	public Metric<int[]> getMetric() {
		return new Manhattan();
	}

	@SuppressWarnings("boxing")
	private Map<Integer, int[]> getMpeg7(File f) throws IOException {
		Map<Integer, int[]> res = new HashMap<>();
		LineNumberReader fr = new LineNumberReader(new FileReader(f));
		boolean finished = false;
		while (!finished) {
			try {
				int id = getIdFromNextLine(fr);
				fr.readLine();
				int[] data = getDataFromNextLines(fr);
				this.dataIds.add(id);
				res.put(id, data);
			} catch (Exception e) {
				System.out.println("finshed: " + e.getMessage());
				finished = true;
			}
		}

		fr.close();
		return res;
	}

	@SuppressWarnings("boxing")
	@Override
	protected Map<Integer, int[]> getRawDataHunk(int fileNumber) {
		Map<Integer, int[]> res = new TreeMap<>();
		int ptr = fileNumber * 1000;
		for (int i = ptr; i < ptr + 1000; i++) {
			int dataId = this.dataIds.get(i);
			res.put(dataId, this.rawData.get(dataId));
		}
		return res;
	}
}
