package eu.similarity.msc.data;

import java.util.Map;
import java.util.TreeMap;

public class MetricSpaceCreation {

	public static void main(String[] args) {
		/*
		 * once these are all created, they can be put in a collection as shown here of
		 * course this will not work until they have been initialised as there is a
		 * requirement to store a lot of data on your local disk!
		 */
		@SuppressWarnings("unused")
		Map<String, MetricSpaceResource<?, ?>> allSpaces = getAllMatureSpaces();
	}

	/**
	 * gets a useful working set of metric space resources. This cannot (usefully!)
	 * be called until such time as these have all been instantiated locally
	 * 
	 * @return a map of metric space resources, indexed by useful names for
	 *         experimental purposes
	 */
	public static Map<String, MetricSpaceResource<?, ?>> getAllMatureSpaces() {
		Map<String, MetricSpaceResource<?, ?>> spaces = new TreeMap<>();
		spaces.put("DeCAF", new DecafMetricSpace("/Volumes/Data/profiset/"));
		spaces.put("GIST", new GistMetricSpace("/Volumes/Data/mf_gist/"));
		spaces.put("MPEG7", new Mpeg7MetricSpace("/Volumes/Data/MPEG7_cnr/"));
		spaces.put("SIFT", new SiftMetricSpace("/Volumes/Data/SIFT_mu/"));
		spaces.put("euc20g", new EucMetricSpace("/Volumes/Data/euc20Gaussian/"));
		spaces.put("euc30g", new EucMetricSpace("/Volumes/Data/euc30Gaussian/"));
		spaces.put("euc30ng", new EucMetricSpace("/Volumes/Data/euc30NonGaussian/"));
		spaces.put("mfAlex", new MfAlexMetricSpace("/Volumes/Data/mf_fc6/"));
		spaces.put("yfccCos", new YfccMetricSpace("/Volumes/Data/yfcc_cnr/"));
		spaces.put("yfccL2", new YfccRawL2Norm("/Volumes/Data/yfcc_cnr/"));
		return spaces;
	}

	/**
	 * Create new Euclidean space data on the local filestore. For each data path,
	 * this should only be called once!
	 * 
	 * @param pathname the local path where the data will be stored, with a trailing
	 *                 "/". You must create the directory first.
	 * @param dim      the dimension of the space
	 * @param gaussian whether each dimension has a Gaussian, or uniform,
	 *                 distribution
	 * @return the new resource
	 */
	@SuppressWarnings("unused")
	public static MetricSpaceResource<Integer, float[]> createEuclidean(String pathname, int dim, boolean gaussian) {
		/*
		 * this is how to create a new Euclidean space, but please note:
		 * 
		 * 1. you must create the directory for the data to be stored before calling
		 * this, and then provide the local path (with a trailing "/") as the first
		 * parameter
		 * 
		 * 2. the call to getData() must be made at the time the notional space is
		 * created, to force the data to be generated; this is really a bug which may be
		 * fixed in future. Unless the dimension is huge, it won't take too long...
		 */
		EucMetricSpace eucMetricSpace = new EucMetricSpace(pathname, dim, gaussian);
		eucMetricSpace.getData();

		/*
		 * all subsequent instantiations should look like this:
		 */
		@SuppressWarnings("unused")
		EucMetricSpace theSameSpace = new EucMetricSpace(pathname);

		return eucMetricSpace;
	}

	/**
	 * Create new YFCC space data on the local filestore. This should only be called
	 * once!
	 */
	/**
	 * @param pathname The pathname where you want the generated space to be stored,
	 *                 with a trailing "/"
	 * @param The      full path to the data file as downloaded from ISTI-CNR
	 * @return The new resource. Subsequent calls should use the other constructor!
	 */

	@SuppressWarnings("unused")
	public static MetricSpaceResource<Long, float[]> createYfcc(String pathname, String dataPathName) {
		/*
		 * this is how to create a new YFCC space based on the public data available
		 * from CNR-ISTI
		 * 
		 * 1. you must create the directory for the data to be stored before calling
		 * this, and then provide the local path (with a trailing "/") as the parameter
		 * 
		 * 2. within this directory, the ground truth file should be available as
		 * 
		 * pathname/GT/hybridCNN_fc6_RawL2Norm_L2_0.txt
		 * 
		 * exactly as spelled here.
		 * 
		 * 3. the second parameter should be the path of the *text* (ie unzipped) file
		 * containing the first million data. This file does not require to be in the
		 * same directory. The file should be called something like
		 * 
		 * YFCC100M_hybridCNN_gmean_fc6_0.txt
		 * 
		 * 4. the data as published is indexed by long and also by string, where the
		 * string can be used to find the image. long-to-string and string-to-long maps
		 * are also constructed during this phase, although not explicitly accessed by
		 * this class, as is a single object file containing all data
		 * 
		 * 5. note that this first call will take a long time as it will read the 30G
		 * text file and write the data back out as 1000 16M object files! Think hours
		 * rather than minutes... maybe days. And use a big heap (-Xmx64g is good...)
		 * 
		 */
		YfccMetricSpace yf = new YfccMetricSpace(pathname, dataPathName);
		// again, you **must** do the call to getData here, otherwise your machine will
		// run hot for half a day and achieve nothing! This is of course undesirable and
		// will be
		// fixed soon...! (or not...!)
		yf.getData();

		/*
		 * all subsequent instantiations should look like this:
		 */
		@SuppressWarnings("unused")
		YfccMetricSpace theSameSpace = new YfccMetricSpace(pathname);

		return yf;
	}

}
