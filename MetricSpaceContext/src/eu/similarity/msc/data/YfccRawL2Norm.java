package eu.similarity.msc.data;

import java.util.List;
import java.util.Map;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.floats.L2Norm;

public class YfccRawL2Norm extends YfccMetricSpace {

	public YfccRawL2Norm(String filePath) {
		super(filePath);
	}

	@Override
	public Map<Long, double[]> getThresholds() {
		throw new RuntimeException("thresholds are not available for this class");
	}

	@Override
	public Metric<float[]> getMetric() {
		return new L2Norm();
	}

	public static void main(String[] a) {
//		YfccRawL2Norm yf = new YfccRawL2Norm("/Volumes/Data/yfcc_cnr/");
		YfccMetricSpace yf = new YfccMetricSpace("/Volumes/Data/yfcc_cnr/");
		// first line in the huge ground truth file...
		final long key = 5029483214L;
		float[] query = yf.getQueries().get(key);	
		List<Long> nns = yf.getNNIds().get(key);
		Map<Long, float[]> data = yf.getData();
		final Metric<float[]> metric = yf.getMetric();
		System.out.print(key);
		for (long nn : nns) {
			System.out.print("\t" + nn + "\t" + metric.distance(query, data.get(nn)));
		}
		System.out.println();
	}
}
