package eu.similarity.msc.data;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.floats.Euclidean;

import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class EucMetricSpace extends IncrementalBuildMetricSpace<Integer, float[]> {

	private int dimension;
	private boolean gaussian;

	private static String DIMENSION_KEY = "dimension";
	private static String GAUSSIAN_KEY = "gaussian";

	public EucMetricSpace(String filePath, int dimension, boolean gaussian) {
		super(filePath);
		this.dimension = dimension;
		this.gaussian = gaussian;
		super.metaData.set(DIMENSION_KEY,dimension);
		super.metaData.set(GAUSSIAN_KEY,gaussian);
		try {
			super.metaData.commit();
		} catch (IOException e) {
			this.logger.severe("cannot commit metaData file");
			throw new RuntimeException(this.getClass().getName());
		}
	}

	public EucMetricSpace(String filePath) {
		super(filePath);
		this.dimension = super.metaData.getInt(DIMENSION_KEY);
		this.gaussian = super.metaData.getBool(GAUSSIAN_KEY);
	}



	@Override
	public Metric<float[]> getMetric() {
		return new Euclidean();
	}

	@SuppressWarnings("boxing")
	@Override
	protected Map<Integer, float[]> getRawDataHunk(int fileNumber) {
		Random rand = new Random(fileNumber);
		Map<Integer, float[]> res = new TreeMap<>();

		for (int i = 0; i < 1000; i++) {
			float[] fs = new float[this.dimension];
			for (int dim = 0; dim < this.dimension; dim++) {
				fs[dim] = (float) (this.gaussian ? rand.nextGaussian() : rand.nextFloat());
			}
			res.put(fileNumber * 1000 + i, fs);
		}
		return res;
	}

	public static void main(String[] a) {
		EucMetricSpace euc = new EucMetricSpace("/Volumes/Data/eucTest/", 0, true);
		euc.getData();
	}

}
