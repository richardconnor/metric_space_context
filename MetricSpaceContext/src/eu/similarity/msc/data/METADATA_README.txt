private static String DIMENSION = "dimension";
private static String GAUSSIAN = "gaussian";
private static String INCLUDES_THRESHOLDS = "includes thresholds";
private static String INCLUDES_NNS = "includes nns";
private static String QUERIES_SUBSET_OF_DATA

private static String NOTES = "notes";              // any notes about space - references to papers etc.
private static String MSC_URL = "msc url"           // the URL in MSC control of where the data can be downloaded
private static String SOURCE_URL = "source url"     // the original URL of the data
private static String PROVENANCE = "provenance"     // relevant free text provenance information

private static String DATA_SIZE = "data size";
private static String NN_SIZE = "nn size"
private static String QUERIES_SIZE = "queries size"
private static String THRESHOLDS_SIZE "thresholds size"

private static String METRIC_NAME = "metric name"
private static String ID_TYPE = "id type"
private static String DATA_REP_TYPE = "data rep type"

//-------------------

this.dimension = metaData.getInt(DIMENSION);
this.gaussian = metaData.getBool(GAUSSIAN);
this.thresholds = metaData.getBool(INCLUDES_THRESHOLDS);
this.nns = metaData.getBool(INCLUDES_NNS);
this.nns = metaData.getBool(QUERIES_SUBSET_OF_DATA);

this.notes = metaData.get(NOTES);
this.msc_url = metaData.get(MSC_URL);
this.source_url = metaData.get(SOURCE_URL);
this.provenance = metaData.get(PROVENANCE);

this.data_size = metaData.getInt(DATA_SIZE);
this.nn_size = metaData.getInt(NN_SIZE);
this.queries_size = metaData.getInt(QUERIES_SIZE);
this.thresholds_size = metaData.getInt(THRESHOLDS_SIZE);

METRIC_NAME
some encoding of ID_TYPE - Java type to String?
some encoding of DATA_REP_TYPE - Java type to String?