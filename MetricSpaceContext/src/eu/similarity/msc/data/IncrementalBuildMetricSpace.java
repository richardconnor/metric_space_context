package eu.similarity.msc.data;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.util.ObjectWithDistance;
import eu.similarity.msc.util.Quicksort;
import eu.similarity.msc.util.MetaData;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;

public abstract class IncrementalBuildMetricSpace<IdType, DataRep> implements MetricSpaceResource<IdType, DataRep> {

	protected MetaData metaData;
	protected String filePath;
	private String queryFilePath;
	private String objectDataPath;
	protected Logger logger;

	public IncrementalBuildMetricSpace(String filePath) {
		this.logger = Logger.getLogger(this.getClass().getName());
		this.filePath = filePath;
		this.queryFilePath = filePath + "queries.obj";
		this.objectDataPath = filePath + "extracted/";
		checkExtractedDir();
		this.metaData = new MetaData(Paths.get(filePath));
	}

	@Override
	public Map<IdType, DataRep> getData() {
		Map<IdType, DataRep> res = new TreeMap<>();
		for (int f = 0; f < 1000; f++) {
			res.putAll(this.getData(f));
		}
		return res;
	}

	public Map<IdType, DataRep> getData(Collection<IdType> required) {
		Map<IdType,DataRep> res = new HashMap<>();
		for( int i = 0; i < 1000; i++) {
			Map<IdType, DataRep> chunk = getData(i);
			for( IdType id : required) {
				if( chunk.keySet().contains(id)) {
					res.put(id, chunk.get(id));
				}
			}
		}
		return res;
	}

	/**
	 * return a single data file of 1k values from the object format. If this
	 * doesn't exist, warn the user and create it
	 * 
	 * @param fileNumber the file to fetch, between 0 and 999 inc.
	 * @return the data from this file
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Map<IdType, DataRep> getData(int fileNumber) {
		final String fileName = this.objectDataPath + fileNumber + ".obj";

		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));

			try {
				@SuppressWarnings("unchecked")
				Map<IdType, DataRep> res = (Map<IdType, DataRep>) ois.readObject();
				ois.close();
				return res;
			} catch (ClassNotFoundException e) {
				ois.close();
				this.logger.severe(
						"cannot read data from " + fileName + ", may be corrupted: try deleting and re-running");
				throw new RuntimeException(this.getClass().getName());
			}
		} catch (FileNotFoundException e) {
			this.logger.info("can't find file " + fileName + "; trying to create it");
			writeObjectDataFile(fileNumber);
			return getData(fileNumber);

		} catch (IOException e) {
			this.logger.severe("cannot open " + fileName + " as input stream, please check file permissions");
			throw new RuntimeException(this.getClass().getName());
		}
	}

	@Override
	public List<IdType> getDataIds() {
		return getSetIds(true);
	}

	@Override
	public List<IdType> getQueryIds() {
		return getSetIds(false);
	}

	private List<IdType> getSetIds(boolean data) {
		try {
			ObjectInputStream oos = new ObjectInputStream(
					new FileInputStream(this.filePath + (data ? "objectIds.obj" : "queryIds.obj")));
			@SuppressWarnings("unchecked")
			List<IdType> res = (List<IdType>) oos.readObject();
			oos.close();
			return res;
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).info("couldn't open file: " + this.filePath
					+ (data ? "objectIds.obj" : "queryIds.obj") + "; trying to create it");
			return populateDataIds(data);
		}
	}

	@Override
	public abstract Metric<DataRep> getMetric();

	@SuppressWarnings("boxing")
	@Override
	public Map<IdType, List<IdType>> getNNIds() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.filePath + "gtNNids.obj"));
			@SuppressWarnings("unchecked")
			// this will fail with ClassCaseException for older instances which have stored
			// files of type
			// Map<Integer,int[]> before the key type was generic

			Map<IdType, List<IdType>> nnids = (Map<IdType, List<IdType>>) ois.readObject();
			// ...but need to actively trigger the array/list duality exception because
			// arrays have no dynamic check...
			@SuppressWarnings("unused")
			IdType dummy = nnids.values().iterator().next().get(0);
			ois.close();

			return nnids;
		} catch (ClassCastException e) {
			// fixing specifically Map<Integer,int[]> to Map<Integer, List<Integer>> issue
			try {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.filePath + "gtNNids.obj"));
				@SuppressWarnings("unchecked")
				Map<Integer, int[]> nnids = (Map<Integer, int[]>) ois.readObject();
				ois.close();

				Map<Integer, List<Integer>> newVersion = new TreeMap<>();
				for (int nnid : nnids.keySet()) {
					List<Integer> l = new ArrayList<>();
					for (int i : nnids.get(nnid)) {
						l.add(i);
					}
					newVersion.put(nnid, l);
				}

				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(this.filePath + "gtNNids_NEW.obj"));
				oos.writeObject(newVersion);
				oos.close();

				// can't return this as Map<IdType,List<IdType>> I don't think!
				Logger.getLogger(this.getClass().getName()).info("have rebuild gtNNids object file, replace it");
				throw new RuntimeException("controlled termination, replace file and restart");
			} catch (IOException | ClassNotFoundException e1) {
				throw new RuntimeException("honey we're fucked, may have to rebuild nearest neighbour info");
			}

		} catch (ClassNotFoundException | IOException e) {
			this.logger.info("can't find file " + this.filePath + "gtNNids.obj; trying to create it");
			return createNNidsFile();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<IdType, DataRep> getQueries() {

		try {
			FileInputStream fis = null;
			ObjectInputStream ois = null;
			Map<IdType, DataRep> res = null;

			fis = new FileInputStream(this.queryFilePath);
			ois = new ObjectInputStream(fis);
			res = (Map<IdType, DataRep>) ois.readObject();
			ois.close();

			return res;
		} catch (FileNotFoundException e) {
			this.logger.info("query data file is not present, going to create it");
			this.logger.info("(this may take a while)");
			writeQueries();
			return getQueries();
		} catch (ClassCastException | IOException e) {
			fatalError(e, "couldn't open nnid file correctly");
			return null;
		} catch (ClassNotFoundException e) {
			fatalError(e, "couldn't open nnid file correctly - no class header");
			return null;
		}
	}

	@Override
	public Map<IdType, double[]> getThresholds() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.filePath + "thresholds.obj"));
			@SuppressWarnings("unchecked")
			Map<IdType, double[]> res = (Map<IdType, double[]>) ois.readObject();
			ois.close();
			return res;
		} catch (FileNotFoundException e) {
			this.logger.info("couldn't open file " + this.filePath + "thresholds.obj");
			this.logger.info("creating new file - may take some time");
			writeThresholdFile();
			return getThresholds();
		} catch (ClassNotFoundException e) {
			fatalError(e, "couldn't open nnid file correctly - no class header");
			return null;
		} catch (IOException e) {
			fatalError(e, "couldn't open nnid file correctly - I/O error");
			return null;
		}

	}

	private void checkExtractedDir() {
		File extracted = new File(this.objectDataPath);
		if (!extracted.exists()) {
			this.logger.info(this.filePath + "extracted/  does not exist; "
					+ (extracted.mkdir() ? " created now" : "unable to create now"));
		}
	}

	private Map<IdType, List<IdType>> createNNidsFile() {
		Map<IdType, DataRep> data = this.getData();
		Map<IdType, DataRep> queries = this.getQueries();
		final Metric<DataRep> metric = this.getMetric();
		Map<IdType, List<IdType>> nnids = new TreeMap<>();
		for (IdType query : queries.keySet()) {
			@SuppressWarnings("unchecked")
			ObjectWithDistance<IdType>[] dists = new ObjectWithDistance[data.size()];
			int ptr = 0;
			for (IdType datum : data.keySet()) {
				final double distance = metric.distance(queries.get(query), data.get(datum));
				dists[ptr++] = new ObjectWithDistance<>(datum, distance);
			}

			Quicksort.placeOrdinal(dists, 100);
			Quicksort.partitionSort(dists, 0, 100);
			List<IdType> newNnids = new ArrayList<>();
			for (int i = 0; i < 100; i++) {
				newNnids.add(dists[i].getValue());
			}
			nnids.put(query, newNnids);
		}

		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.filePath + "gtNNids.obj"));
			oos.writeObject(nnids);
			oos.close();
		} catch (FileNotFoundException e) {
			fatalError(e, "couldn't create file: " + this.filePath + "gtNNids.obj");
		} catch (IOException e) {
			fatalError(e, "couldn't write to file: " + this.filePath + "gtNNids.obj");
		}

		return nnids;
	}

	private void fatalError(Exception e, String message) {
		this.logger.severe(message);
		throw new RuntimeException(e.toString());
	}

	@SuppressWarnings({ "boxing", "unused" })
	private Set<Integer> getPivotIds() {
		Collection<IdType> queries = generateQueryIds();
		Set<Integer> res = new TreeSet<>();
		// nb Random is seeded so result is deterministic
		Random rand = new Random(0);
		while (res.size() < 1000) {
			int next = rand.nextInt(1000 * 1000);
			if (!queries.contains(next)) {
				res.add(next);
			}
		}
		return res;
	}

	/**
	 * @return the set of query ids in the case where one is not pre-defined
	 */
	protected List<IdType> generateQueryIds() {
		List<IdType> res = new ArrayList<>();
		// nb Random is seeded so result is deterministic
		Random rand = new Random(0);
		List<IdType> dids = getDataIds();
		while (res.size() < 1000) {
			int next = rand.nextInt(1000 * 1000);
			res.add(dids.get(next));
		}
		return res;
	}

	/**
	 * requires only getData to be operative
	 * 
	 * @return a list of data ids, by side-effect create this as a Java object file
	 */
	protected List<IdType> populateDataIds(boolean isData) {
		List<IdType> dataIds = new ArrayList<>();
		Map<IdType, DataRep> data = isData ? this.getData() : this.getQueries();
		for (IdType i : data.keySet()) {
			dataIds.add(i);
		}
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(this.filePath + (isData ? "objectIds.obj" : "queryIds.obj")));
			oos.writeObject(dataIds);
			oos.close();
		} catch (IOException e) {
			throw new RuntimeException();
		}
		return dataIds;
	}

	private void writeObjectDataFile(int fileNumber) {
		Map<IdType, DataRep> hunk = getRawDataHunk(fileNumber);

		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(this.objectDataPath + fileNumber + ".obj"));
			oos.writeObject(hunk);
			oos.close();
		} catch (FileNotFoundException e) {
			this.logger.severe("can't open file " + this.objectDataPath + fileNumber + ".obj");
			throw new RuntimeException(this.getClass().getName());
		} catch (IOException e) {
			this.logger.severe("can't write to file " + this.objectDataPath + fileNumber + ".obj");
			throw new RuntimeException(this.getClass().getName());
		}
	}

	@SuppressWarnings("unused")
	private void writeObjectDataFiles() throws IOException {
		for (int i = 0; i < 1000; i++) {
			writeObjectDataFile(i);
		}
	}

	/**
	 * requires getData() and getQueryIds(); the latter generated by default if not
	 * overridden
	 * 
	 * @return the queries, having first written them to a Java object file by
	 *         side-effect
	 */
	private Map<IdType, DataRep> writeQueries() {
		try {
			Map<IdType, DataRep> queries = new TreeMap<>();
			Map<IdType, DataRep> data = getData();
			Collection<IdType> queryIds = generateQueryIds();
			for (IdType qid : queryIds) {
				queries.put(qid, data.get(qid));
			}

			final FileOutputStream fos = new FileOutputStream(this.queryFilePath);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(queries);
			oos.close();

			return queries;
		} catch (FileNotFoundException e) {
			fatalError(e, "couldn't create file output stream " + this.queryFilePath);
			return null;
		} catch (IOException e) {
			fatalError(e, "I/O error in writing file " + this.queryFilePath);
			return null;
		}

	}

	private void writeThresholdFile() {
		try {
			Map<IdType, double[]> res = new TreeMap<>();
			Map<IdType, List<IdType>> nnids = this.getNNIds();
			Map<IdType, DataRep> data = this.getData();
			Map<IdType, DataRep> queries = this.getQueries();
			final Metric<DataRep> metric = this.getMetric();
			for (IdType qid : queries.keySet()) {
				DataRep query = queries.get(qid);
				List<IdType> nns = nnids.get(qid);
				double[] dists = new double[100];
				int ptr = 0;
				for (IdType nn : nns) {
					dists[ptr++] = metric.distance(query, data.get(nn));
				}
				res.put(qid, dists);
			}

			final FileOutputStream fos = new FileOutputStream(this.filePath + "thresholds.obj");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(res);
			oos.close();
		} catch (FileNotFoundException e) {
			fatalError(e, "couldn't create file output stream " + this.filePath + "thresholds.obj");
		} catch (IOException e) {
			fatalError(e, "I/O error in writing file " + this.filePath + "thresholds.obj");
		}
	}

	protected abstract Map<IdType, DataRep> getRawDataHunk(int fileNumber);

	@Override
	public Dictionary getMetaData() {
		return this.metaData;
	}

}
