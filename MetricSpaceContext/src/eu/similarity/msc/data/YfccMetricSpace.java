package eu.similarity.msc.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.floats.Cosine;

public class YfccMetricSpace extends IncrementalBuildMetricSpace<Long, float[]> {

	private Map<Long, float[]> allData;
	private Map<String, Long> strToLongMap;
	private Map<Long, String> longToStrMap;
	private List<Long> objectIds;

	public YfccMetricSpace(String filePath) {
		super(filePath);
	}

	@SuppressWarnings("boxing")
	public YfccMetricSpace(String filePath, String rawFilePath) {
		super(filePath);
		this.allData = new TreeMap<>();
		this.strToLongMap = new TreeMap<>();
		this.longToStrMap = new TreeMap<>();
		this.objectIds = new ArrayList<>();
		try {
			FileReader file = new FileReader(rawFilePath);
			LineNumberReader lnr = new LineNumberReader(file);
			if (!lnr.readLine().equals("")) {
				lnr.close();
				throw new RuntimeException();
			}

			int count = 0;
			for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
				Scanner s = new Scanner(line);
				long id1 = s.nextLong();
				String id2 = s.next();
				this.objectIds.add(id1);
				this.strToLongMap.put(id2, id1);
				this.longToStrMap.put(id1, id2);

				float[] data = new float[4096];
				for (int i = 0; i < 4096; i++) {
					data[i] = s.nextFloat();
				}
				this.allData.put(id1, data);

				if (s.hasNext()) {
					s.close();
					throw new RuntimeException("oh shite");
				}
				s.close();
				count++;
				if (count % 1000 == 0) {
					System.out.print(".");
				}
			}
			lnr.close();
			System.out.println(count + " lines read");
			System.out.println(this.allData.size());

			storeInFile("strToLongMap.obj", this.strToLongMap);
			storeInFile("longToStrMap.obj", this.longToStrMap);

		} catch (FileNotFoundException e) {
			throw new RuntimeException("oh shite");
		} catch (IOException e) {
			throw new RuntimeException("oh shite");
		}
	}

	private void storeInFile(final String filename, Object storand) throws FileNotFoundException, IOException {
		FileOutputStream oos = new FileOutputStream(this.filePath + filename);
		ObjectOutputStream fos = new ObjectOutputStream(oos);
		fos.writeObject(storand);
		fos.close();
	}

	@SuppressWarnings("boxing")
	@Override
	public Map<Long, List<Long>> getNNIds() {
		Map<Long, List<Long>> res = new TreeMap<>();
		try {
			LineNumberReader lnr = new LineNumberReader(
					new FileReader(this.filePath + "GT/hybridCNN_fc6_RawL2Norm_L2_0.txt"));
			for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
				Scanner s = new Scanner(line);
				String ids = s.next();
				String[] idsVec = ids.split("_");
				long id = Long.parseLong(idsVec[0]);
				ArrayList<Long> nnids = new ArrayList<>();
				while (s.hasNext() && nnids.size() < 100) {
					String clump = s.next();
					String[] clumps = clump.split(",");
					long nnid = Long.parseLong(clumps[0].split("_")[0]);
					nnids.add(nnid);
				}
				res.put(id, nnids);

				s.close();
			}
			lnr.close();
		} catch (FileNotFoundException e) {
			// can't create lnr
			throw new RuntimeException("oh shite");
		} catch (IOException e) {
			// lnr failure
			throw new RuntimeException("oh shite");
		}
		return res;
	}
	
	public void nothing() {
		
	}

	@Override
	public Metric<float[]> getMetric() {
		return new Cosine();
	}

	/**
	 * this gets a raw data hunk
	 */
	@SuppressWarnings("boxing")
	@Override
	protected Map<Long, float[]> getRawDataHunk(int fileNumber) {
		Map<Long, float[]> res = new TreeMap<>();
		int ptr = fileNumber * 1000;
		for (int i = ptr; i < ptr + 1000; i++) {
			long dataId = this.objectIds.get(i);
			res.put(dataId, this.allData.get(dataId));
		}
		return res;
	}

	public Map<Long, String> getStringIndex() {
		try {
			FileInputStream fis = new FileInputStream(this.filePath + "longToStrMap.obj");
			ObjectInputStream ois = new ObjectInputStream(fis);
			@SuppressWarnings("unchecked")
			Map<Long, String> res = (Map<Long, String>) ois.readObject();
			ois.close();
			return res;
		} catch (Exception e) {
			throw new RuntimeException("sorry can't find long to string map");
		}
	}

}
