package eu.similarity.msc.data;

import eu.similarity.msc.core_concepts.Metric;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestAllMetricSpaceResources {

	public static final String ROOT_DIR = "/Volumes/Data/";

	public static final String MPEG_7_CNR_PATH = ROOT_DIR + "MPEG7_cnr/";
	public static final String YFCC_CNR_PATH = ROOT_DIR + "yfcc_cnr/";
	public static final String EUC_20_GAUSSIAN_PATH = ROOT_DIR + "euc20Gaussian/";
	public static final String MF_GIST_PATH = ROOT_DIR + "mf_gist/";
	public static final String PROFISET_PATH = ROOT_DIR + "profiset/";
	public static final String SIFT_MU_PATH = ROOT_DIR + "SIFT_mu/";
	public static final String MF_FC_6_PATH = ROOT_DIR + "mf_fc6/";
	private static final double EPSILON = Double.MIN_VALUE;

	/**
	 * Zero param constructor required by Junit.
	 */
	public TestAllMetricSpaceResources() {}

	@Test
	public void testAll() throws ClassNotFoundException, IOException {
//		testFC6();
//		testSIFTMu();
//		testProfiset();
//		testGIST();
		testEuc20();
//		testYFCC();
//		testMPEG7();
	}

	private static void testMPEG7() throws IOException, ClassNotFoundException {
		IncrementalBuildMetricSpace<Integer, int[]> mpeg7 = new Mpeg7MetricSpace(MPEG_7_CNR_PATH);
		testResource(mpeg7);
	}

	private static void testYFCC() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Long, float[]> yfcc = new YfccMetricSpace(YFCC_CNR_PATH);
		testResource(yfcc);
	}

	private static void testEuc20() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Integer, float[]> euc20 = new EucMetricSpace(EUC_20_GAUSSIAN_PATH,20,true);
		testResource(euc20);
	}

	private static void testGIST() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Integer, float[]> gist = new GistMetricSpace(MF_GIST_PATH);
		testResource(gist);
	}

	private static void testProfiset() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Integer, float[]> profiset = new DecafMetricSpace(PROFISET_PATH);
		testResource(profiset);
	}

	private static void testSIFTMu() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Integer, float[]> gsd = new SiftMetricSpace(SIFT_MU_PATH);
		testResource(gsd);
	}

	private static void testFC6() throws IOException, ClassNotFoundException {
		MetricSpaceResource<Integer, float[]> mf = new MfAlexMetricSpace(MF_FC_6_PATH);
		testResource(mf);
	}


	public static <Index, Representation> void testResource(MetricSpaceResource<Index, Representation> resource)
			throws IOException, ClassNotFoundException {

		Map<Index, Representation> data = resource.getData();
		List<Index> data_ids = resource.getDataIds();
		checkIds( data_ids, data );

		Map<Index, Representation> queries = resource.getQueries();
		List<Index> query_ids = resource.getQueryIds();
		checkIds( query_ids, queries );

		Map<Index, List<Index>> nnids = resource.getNNIds();
		assert( nnids.size() >= 0 );

		Map<Index, double[]> nnThresholds = resource.getThresholds();
		assert( nnThresholds.size() >= 0 );

		checkThresholds(resource.getMetric(), queries, nnThresholds, nnids, data);
	}

	private static <Index, Representation> void checkIds(List<Index> ids, Map<Index,Representation> contents) {
		assert( contents.size() >= 0 );
		assert( ids.size() == contents.size() );
		for( Index i : ids ) {
			Representation r = contents.get(i);
			assertNotNull(r);
		}
	}

	private static <Index, Representation> void checkThresholds(Metric<Representation> metric, Map<Index, Representation> queries, Map<Index, double[]> nnThresholds, Map<Index, List<Index>> nnids, Map<Index, Representation> data) {
		for (Index qId : queries.keySet()) {

			final double[] thresholds = nnThresholds.get(qId);
			final int size = thresholds.length;
			final double topThreshold = thresholds[size - 1];

			double last_dist = 0;
			for( int i = 0; i < size - 1; i++ ) {

				Index nnId = nnids.get(qId).get(i); // the next nn id in the dataset

				final double next_thresh = nnThresholds.get(qId)[i];
				assert (next_thresh < topThreshold); // is less than the furthest
				assert (next_thresh >= last_dist);   // is further than or same as last one

				double dist = metric.distance(queries.get(qId), data.get(nnId)); // independently measure distance

				assertEquals(dist, next_thresh, EPSILON);  // and the same as the result of the query.

				last_dist = dist;
			}
		}
	}
}
