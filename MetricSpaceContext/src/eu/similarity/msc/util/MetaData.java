/*
 * Copyright 2020 Systems Research Group, University of St Andrews:
 * <https://github.com/stacs-srg>
 */
package eu.similarity.msc.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Loads space parameters from external metaData file.
 */
public class MetaData extends Properties {

    private static final long serialVersionUID = 15472546L;
    private static final String APPLICATION_PROPERTIES_FILE_NAME = "metaData.txt";
    private static final String COMMENTS = "Metric Space metaData updated: ";

    private final Path base_path;
    private final Logger logger;

    public MetaData(Path base_path) {

        this.base_path = base_path;
        this.logger = Logger.getLogger(this.getClass().getName());

        try (InputStream input_stream = Files.newInputStream(base_path.resolve(APPLICATION_PROPERTIES_FILE_NAME))) {
            super.load(input_stream);
        }
        catch (IOException e) {
            try {
                this.logger.info("can't find file " + APPLICATION_PROPERTIES_FILE_NAME + "; trying to create it");
                Files.createFile(base_path.resolve(APPLICATION_PROPERTIES_FILE_NAME));
            } catch (IOException e1) {
                throw new RuntimeException("Could not create metaData: " + e.getMessage());
            }
        }
    }

    public int getInt( String key ) {
        final String val_as_string = super.getProperty(key);
        return val_as_string == null ? 0 : Integer.parseInt(val_as_string);
    }

    public float getFloat( String key ) {
        final String val_as_string = super.getProperty(key);
        return val_as_string == null ? 0.0f : Float.parseFloat(val_as_string);
    }

    public double getDouble( String key ) {
        final String val_as_string = super.getProperty(key);
        return val_as_string == null ? 0.0 : Double.parseDouble(val_as_string);
    }

    public boolean getBool( String key ) {
        final String val_as_string = super.getProperty(key);
        return val_as_string == null ? false : Boolean.parseBoolean(val_as_string);
    }

    public void set( String key, int value ) {
        this.setProperty( key, Integer.toString(value) );
    }

    public void set( String key, float value ) {
        this.setProperty( key, Float.toString(value) );
    }

    public void set( String key, double value ) {
        this.setProperty( key, Double.toString(value) );
    }

    public void set( String key, boolean value ) {
        this.setProperty( key, Boolean.toString(value) );
    }

    public void commit() throws IOException {

        try( OutputStream out = Files.newOutputStream(base_path.resolve(APPLICATION_PROPERTIES_FILE_NAME)) ) {

            super.store(out,COMMENTS + new Date().toString());
        }
    }
}
