package eu.similarity.msc.util;

import eu.similarity.msc.core_concepts.Metric;

/**
 * 
 * distance is the Euclidean distance between the end points of normalised
 * vectors
 * 
 * a variant proposed by Lucia Vadicamo, documented in arXiv:1604.08640
 * 
 * @author Richard Connor
 * 
 */
public class CosineFp<T extends CartesianPointL2Norm> implements
		Metric<T> {

	private static double oneOverRoot2 = 1 / Math.sqrt(2);

	@Override
	public double distance(T point1, T point2) {

		double[] p1 = point1.getPoint();
		double[] p2 = point2.getPoint();

		double magAcc = 0;
		for (int i = 0; i < p1.length; i++) {
			final double p1n = p1[i];
			final double p2n = p2[i];
			final double p12diff = p1n - p2n;
			magAcc += p12diff * p12diff;
		}

		return Math.sqrt(magAcc) * oneOverRoot2;
	}

	@Override
	public String getMetricName() {
		return "cos";
	}

	public static double convertFromCosine(double cos) {
		double ang = cos * (Math.PI / 2);
		return 2 * Math.sin(ang / 2) * oneOverRoot2;
	}

}
