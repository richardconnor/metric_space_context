package eu.similarity.msc.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.metrics.floats.Euclidean;

public class GistFileReader {

	public static final int N_SCALES = 6;
	public static final int N_ORIENTATIONS = 5;
	public static final int N_WINDOWS = 4;
	public static final int BYTES_PER_FLOAT = 4;

	public static final int GIST_SIZE = N_SCALES * N_ORIENTATIONS * N_WINDOWS * N_WINDOWS;

	public static final int GIST_SIZE_IN_BYTES = GIST_SIZE * BYTES_PER_FLOAT;

	protected static final int BUFFER_SIZE = 2048;

	private float[] gist_values;
	private double[] gist_doubles;
	private String filename;

	public GistFileReader(String filename) throws IOException {
		this.filename = filename;
		FileInputStream in = new FileInputStream(filename);
		readData(in, false);
		in.close();
	} // GistFileReader

	public float[] getGistValues() {
		return this.gist_values;
	}

	public float[] getGistValues_l1Normed() throws Exception {
		return normalise_l1(this.gist_values);
	}

	private void readData(FileInputStream in, boolean normalised) throws IOException {
		byte[] bytes = new byte[BUFFER_SIZE];

		int nBytes = in.read(bytes);

		if (nBytes != GIST_SIZE_IN_BYTES) {
			throw new IOException("Wrong number of bytes");
		}

		parseBytes(bytes, normalised);
	}

	protected void parseBytes(byte[] bytes, boolean normalised) {
		this.gist_values = new float[GIST_SIZE];
		this.gist_doubles = new double[GIST_SIZE];

		ByteBuffer buf = ByteBuffer.wrap(bytes);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		int gist_idx = 0;

		for (int i = 0; i < GIST_SIZE_IN_BYTES; i += 4) {
			final float val = buf.getFloat(i);
			this.gist_values[gist_idx] = val;
			this.gist_doubles[gist_idx] = val;

			gist_idx++;
		}
	}

	private float[] normalise_l1(float[] fs) throws Exception {
		float[] res = new float[fs.length];
		float acc = 0;
		for (float f : fs) {
			if (acc < 0) {
				throw new Exception("negative value in GIST rep");
			}
			if (Float.isNaN(f)) {
				throw new Exception("NaN value in GIST rep");
			}
			acc += f;
		}
		if (acc == 0) {
			throw new Exception("all values are zero in GIST rep");
		}
		for (int i = 0; i < fs.length; i++) {
			final float f = fs[i] / acc;
			res[i] = f;
		}
		return res;
	}

}
